<? Php
$ Start = microtime (true);
require_once 'src / func.php'; / * Include functions * /
require_once 'src / widget.php'; / * Specific functions for widgets * /
require_once 'src / clientname-dashboard.php'; / * Clientname handling functions * /
require_once 'src / policy-dashboard.php'; / * Functions for managing policies * /
require_once 'src / images-dashboard.php'; / * functions for handling images * /
require_once 'src / errors-dashboard.php'; / * Bperror handling functions * /
starthead (); / * I include the header of the page * /
style_menu ();
style_dashboard (); / * Style dashboard menu * /
onloadDashboard ();
endhead ();
loadWait ();
menudashboard ();
?>
<Ul class = "headnote">
The following data are separated from the system due to be static that require unnecessary processing capacity at each refresh.
</ Ul>
<Ul id = "myDashboard"> </ ul>

<Script type = "text / javascript">
$ (Function () {
// Theme switcher plugin
$ ("# Switcher"). Themeswitcher ({
imgPath: "css / images /"
loadTheme "Blitzer"
});

// // **********************************************
// Json dashboard data
// This is the data format Expects That the dashboard framework
// // **********************************************

var dashboardJSON = [{
<? Php
Clientname_table_ds $ c = new ();
$ C-> clientname_table_ds__data ();
$ C-> clientname_table_ds__table ();
?>
}, {
<? Php
Clientname_oslike_ds $ c = new ();
$ C-> clientname_oslike_ds__data ();
$ C-> clientname_graph_os_ds__draw ();
?>
}, {
<? Php
Policy_active_ds $ p = new (); // Dashboard policy active / inactive
$ Params = array ('name' => 'policy_active_ds', 'policy' => 'Any', 2);
$ P-> policy_active_ds__data ($ params);
$ P-> policy_active_ds__draw ($ params);
?>
}, {
<? Php
Policy_type_like_ds $ p = new (); // Type Defined Policy
$ P-> policy_type_like_ds__data ();
 $ P-> policy_type_like_ds__draw ();
?>
}];

// Example basic initialization
$ ("# MyDashboard"). SDashboard ({
dashboardData: dashboardJSON,
disableSelection: false // Enables text selection on the dashboard

});

// Table row clicked event example
$ ("# MyDashboard"). Bind ("sdashboardrowclicked", function (e, data) {
$ .gritter.add ({
position: 'bottom-left',
title: 'Table Click'
time: 1000,
text: 'click options widget mode table are not available'
});

if (console) {
console.log ("table row clicked, for widget" + data.selectedWidgetId);
}
});

// Plot selected event example
$ ("# MyDashboard"). Bind ("sdashboardplotselected", function (e, data) {
$ .gritter.add ({
position: 'bottom-left',
title: 'Selection Box'
time: 1000,
text: 'The selection options in plot mode widget not available'
});
if (console) {
console.log ("chart range selected, for widget" + data.selectedWidgetId);
}
});
// Plot example click event
$ ("# MyDashboard"). Bind ("sdashboardplotclicked", function (e, data) {
$ .gritter.add ({
position: 'bottom-left',
title: 'Click Plot'
time: 1000,
text: 'click options widget plot mode is not available'
});
if (console) {
console.log ("chart clicked, for widget" + data.selectedWidgetId);
}
});

// Order Changes event widget example
$ ("# MyDashboard"). Bind ("sdashboardorderchanged", function (e, data) {
$ .gritter.add ({
position: 'bottom-left',
title: 'Order Widget'
time: 4000,
text: 'It has changed the order of the widgets. Check for more information console.log '
});
if (console) {
console.log ("Sorted Array");
console.log ("+++++++++++++++++++++++++");
console.log (data.sortedDefinitions);
console.log ("+++++++++++++++++++++++++");
}

});

});
</ Script>
<? Php
footer ();
$ End = microtime (true);
print "<div id = 'footer' class = 'footer'> Page generated in" .round (($ end - $ start), 4) "seconds </ div>.";
?>
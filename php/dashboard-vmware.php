<? Php 
  $ Start = microtime (true); 
  require_once 'src / func.php'; / * include functions * / 
  require_once 'src / widget.php'; / * specific functions for widgets * / 
  require_once 'src / clientname-dashboard.php'; / * clientname handling functions * / 
  require_once 'src / policy-dashboard.php'; / * functions for managing policies * / 
  require_once 'src / images-dashboard.php'; / * functions for handling images * / 
  require_once 'src / errors-dashboard.php'; / * bperror handling functions * / 
  starthead (); / * include the header of the page * / 
  style_menu (); 
  style_dashboard (); / * dashboard style menu * / 
  onloadDashboard (); 
  endhead (); 
  loadWait (); 
  menudashboard (); 
  ?> 
  <Ul class = "headnote"> 
  The following graphic applies only to VMware environments.  If the information sought is not found or is insufficient it is suggested to use the specific function outside the dashboard. 
  </ Ul> 
  <Ul id = "myDashboard" class = "myDashboard"> </ ul> 
  <? Php  
  Images_ConsumeHistoricAllDaily $ i = new (); 
  $ Params = array ('name' => 'images_ConsumeHistoricAllDaily_24h', 'policy' => 'vmware', 'timeago' => '24 Hour', 'type' => 'hour', 4); 
  $ I -> images_ConsumeHistoricAllDaily__params ($ params); 
  $ I -> images_ConsumeHistoricAllDaily__data ($ params); 
  $ I -> images_ConsumeHistoricAllDaily__js ($ params); 
  $ Params = array ('name' => 'images_ConsumeHistoricAllDaily_7d', 'policy' => 'vmware', 'timeago' => '7 Day', 'type' => 'day', 4); 
  $ I -> images_ConsumeHistoricAllDaily__params ($ params); 
  $ I -> images_ConsumeHistoricAllDaily__data ($ params); 
  $ I -> images_ConsumeHistoricAllDaily__js ($ params); 
  $ Params = array ('name' => 'images_ConsumeHistoricAllDaily_14d', 'policy' => 'vmware', 'timeago' => '14 Day', 'type' => 'day', 4); 
  $ I -> images_ConsumeHistoricAllDaily__params ($ params); 
  $ I -> images_ConsumeHistoricAllDaily__data ($ params); 
  $ I -> images_ConsumeHistoricAllDaily__js ($ params); 
  ?> 
  <Script type = "text / javascript"> 
  $ (Function () { 
  // Theme switcher plugin 
  $ ("#switcher") .themeswitcher ({ 
  imgPath: "css / images /" 
  loadTheme "Blitzer" 
  }); 
  // // ********************************************** 
  // Json dashboard data 
  // This is the data format Expects That the dashboard framework 
  // // ********************************************** 
  var dashboardJSON = [{ 
  <? Php   
  Policy_active_ds $ p = new (); // dashboard policy active / inactive 
  $ Params = array ('name' => 'policy_active_ds', 'policy' => 'vmware', 2); 
  $ P -> policy_active_ds__data ($ params); 
  $ P -> policy_active_ds__draw ($ params); 
 					  ?> 
  }, { 
  <? Php  
  $ E = new errors_StatusLike_ds; //% of return status code like - multiple params possible 
  $ Params = array ('name' => 'errors_statuslike_ds_24h', 'policy' => 'vmware', 'timeago' => '24 Hour', 3); 
  $ E -> errors_StatusLike_ds__data ($ params); 
  $ E -> errors_StatusLike_ds__graph ($ params); 
 					  ?> 
  }, { 
  <? Php  
  $ E = new errors_StatusLike_ds; //% of return status code like - multiple params possible 
  $ Params = array ('name' => 'errors_StatusLike_ds_7d', 'policy' => 'vmware', 'timeago' => '7 Day', 3); 
  $ E -> errors_StatusLike_ds__data ($ params); 
  $ E -> errors_StatusLike_ds__graph ($ params); 
 					  ?> 
  }, { 
  <? Php  
  $ E = new errors_StatusLike_ds; //% of return status code like - multiple params possible 
  $ Params = array ('name' => 'errors_StatusLike_ds_14d', 'policy' => 'vmware', 'timeago' => 'Day 14', 3); 
  $ E -> errors_StatusLike_ds__data ($ params); 
  $ E -> errors_StatusLike_ds__graph ($ params); 
 					  ?> 
  }, { 
  <? Php   
  Errors_StatusLikeDefined_ds $ e = new (); //% of return status code not like 0 - multiple params possible 
  $ Params = array ('name' => 'errors_StatusLikeDefined_ds_24h', 'policy' => 'vmware', 'timeago' => '24 Hour', 3); 
  $ E -> errors_StatusLikeDefined_ds__data ($ params); 
  $ E -> errors_StatusLikeDefined_ds__graph ($ params); 
 					  ?> 
  }, { 
  <? Php   
  Errors_StatusLikeDefined_ds $ e = new (); //% of return status code not like 0 - multiple params possible 
  $ Params = array ('name' => 'errors_StatusLikeDefined_ds_7d', 'policy' => 'vmware', 'timeago' => '7 Day', 3); 
  $ E -> errors_StatusLikeDefined_ds__data ($ params); 
  $ E -> errors_StatusLikeDefined_ds__graph ($ params); 
 					  ?> 
  }, { 
  <? Php   
  Errors_StatusLikeDefined_ds $ e = new (); //% of return status code not like 0 - multiple params possible 
  $ Params = array ('name' => 'errors_StatusLikeDefined_ds_14d', 'policy' => 'vmware', 'timeago' => 'Day 14', 3); 
  $ E -> errors_StatusLikeDefined_ds__data ($ params); 
  $ E -> errors_StatusLikeDefined_ds__graph ($ params); 
 					  ?> 
  }, { 
  <? Php   
  Errors_FailedList_ds $ e = new (); // List not return status of 0 - multiple params possible 
  $ Params = array ('name' => 'errors_FailedList_ds_24h', 'policy' => 'vmware', 'timeago' => '24 Hour', 3); 
  $ E -> errors_FailedList_ds__data ($ params); 
  $ E -> errors_FailedList_ds__table ($ params); 
 					  ?> 
  }, { 
  <? Php   
  Errors_FailedList_ds $ e = new (); // List not return status of 0 - multiple params possible 
  $ Params = array ('name' => 'errors_FailedList_ds_7d', 'policy' => 'vmware', 'timeago' => '7 Day', 3); 
  $ E -> errors_FailedList_ds__data ($ params); 
  $ E -> errors_FailedList_ds__table ($ params); 
 					  ?> 
  }, { 
  <? Php   
  Errors_FailedList_ds $ e = new (); // List not return status of 0 - multiple params possible 
  $ Params = array ('name' => 'errors_FailedList_ds_14d', 'policy' => 'vmware', 'timeago' => 'Day 14', 3); 
  $ E -> errors_FailedList_ds__data ($ params); 
  $ E -> errors_FailedList_ds__table ($ params); 
 					  ?> 
  }, { 
  <? Php   
  Images_RetentionLevelList_ds $ i = new (); // List of retention level - multiple params possible 
  $ Params = array ('name' => 'images_RetentionLevelList_ds_24h', 'policy' => 'vmware', 'timeago' => '24 Hour', 3); 
  $ I -> images_RetentionLevelList_ds__data ($ params); 
  $ I -> images_RetentionLevelList_ds__graph ($ params); 
 					  ?> 
  }, { 
  <? Php   
  Images_RetentionLevelList_ds $ i = new (); // List of retention level - multiple params possible 
  $ Params = array ('name' => 'images_RetentionLevelList_ds_7d', 'policy' => 'vmware', 'timeago' => '7 Day', 3); 
  $ I -> images_RetentionLevelList_ds__data ($ params); 
  $ I -> images_RetentionLevelList_ds__graph ($ params); 
 					  ?> 
  }, { 
  <? Php   
  Images_RetentionLevelList_ds $ i = new (); // List of retention level - multiple params possible 
  $ Params = array ('name' => 'images_RetentionLevelList_ds_14d', 'policy' => 'vmware', 'timeago' => 'Day 14', 3); 
  $ I -> images_RetentionLevelList_ds__data ($ params); 
  $ I -> images_RetentionLevelList_ds__graph ($ params); 
 					  ?> 
  }, { 
  <? Php   
 Images_ClientImageInImages_ds $ i = new (); // List of clients in images - multiple params possible 
  $ Params = array ('name' => 'images_ClientImageInImages_ds_24h', 'client' => 'vmware', 'timeago' => '24 Hour', 3); 
  $ I -> images_ClientImageInImages_ds__data ($ params); 
  $ I -> images_ClientImageInImages_ds__graph ($ params); 
 					  ?> 
  }, { 
  <? Php   
  Images_ConsumeClientList_ds $ i = new (); // consume List of clients - multiple params possible 
  $ Params = array ('name' => 'images_ConsumeClientList_ds_24h', 'policy' => 'vmware', 'timeago' => '24 Hour', 3); 
  $ I -> images_ConsumeClientList_ds__data ($ params); 
  $ I -> images_ConsumeClientList_ds__table ($ params); 
 					  ?> 
  }, { 
  <? Php   
  Images_ConsumeClientList_ds $ i = new (); // consume List of clients - multiple params possible 
  $ Params = array ('name' => 'images_ConsumeClientList_ds_7d', 'policy' => 'vmware', 'timeago' => '7 Day', 3); 
  $ I -> images_ConsumeClientList_ds__data ($ params); 
  $ I -> images_ConsumeClientList_ds__table ($ params); 
 					  ?> 
  }, { 
  <? Php   
  Images_ConsumeClientList_ds $ i = new (); // consume List of clients - multiple params possible 
  $ Params = array ('name' => 'images_ConsumeClientList_ds_14d', 'policy' => 'vmware', 'timeago' => 'Day 14', 3); 
  $ I -> images_ConsumeClientList_ds__data ($ params); 
  $ I -> images_ConsumeClientList_ds__table ($ params); 
 					  ?> 
  }, { 
  <? Php   
  Images_ConsumePolicyList_ds $ i = new (); 
  $ Params = array ('name' => 'images_ConsumePolicyList_ds_24h', 'policy' => 'vmware', 'timeago' => '24 Hour', 3); 
  $ I -> images_ConsumePolicyList_ds__data ($ params); 
  $ I -> images_ConsumePolicyList_ds__table ($ params); 
 					  ?> 
  }, { 
  <? Php   
  Images_ConsumePolicyList_ds $ i = new (); 
  $ Params = array ('name' => 'images_ConsumePolicyList_ds_7d', 'policy' => 'vmware', 'timeago' => '7 Day', 3); 
  $ I -> images_ConsumePolicyList_ds__data ($ params); 
  $ I -> images_ConsumePolicyList_ds__table ($ params); 
 					  ?> 
  }, { 
  <? Php   
  Images_ConsumePolicyList_ds $ i = new (); 
  $ Params = array ('name' => 'images_ConsumePolicyList_ds_14d', 'policy' => 'vmware', 'timeago' => 'Day 14', 3); 
  $ I -> images_ConsumePolicyList_ds__data ($ params); 
  $ I -> images_ConsumePolicyList_ds__table ($ params); 
 					  ?> 
  }, { 
  <? Php  
  Images_ListGroupHour_ds $ i = new (); 
  $ Params = array ('name' => 'images_ListGroupHour_ds_24h', 'policy' => 'vmware', 'timeago' => '24 Hour', 3); 
  $ I -> images_ListGroupHour_ds__data ($ params); 
  $ I -> images_ListGroupHour_ds__table ($ params); 
 					  ?> 
  }, { 
  <? Php   
  Images_ListGroupHour_ds $ i = new (); 
  $ Params = array ('name' => 'images_ListGroupHour_ds_7d', 'policy' => 'vmware', 'timeago' => '7 Day', 3); 
  $ I -> images_ListGroupHour_ds__data ($ params); 
  $ I -> images_ListGroupHour_ds__table ($ params); 
 					  ?> 
  }, { 
  <? Php   
  Images_ListGroupHour_ds $ i = new (); 
  $ Params = array ('name' => 'images_ListGroupHour_ds_14d', 'policy' => 'vmware', 'timeago' => 'Day 14', 3); 
  $ I -> images_ListGroupHour_ds__data ($ params); 
  $ I -> images_ListGroupHour_ds__table ($ params); 
 					  ?> 
  }, { 
  <? Php  
  Images_PolicyLikeList_ds $ i = new (); // List like pie consumed by Policy - multiple params possible 
  $ Params = array ('name' => 'images_PolicyLikeList_ds_24h', 'policy' => 'vmware', 'timeago' => '24 Hour', 3); 
  $ I -> images_ClientLikeList_ds__data ($ params); 
  $ I -> images_ClientLikeList_ds__graph ($ params); 
 					  ?> 
  }, { 
  <? Php  
  Images_PolicyLikeList_ds $ i = new (); // List like pie consumed by Policy - multiple params possible 
  $ Params = array ('name' => 'images_PolicyLikeList_ds_7d', 'policy' => 'vmware', 'timeago' => '7 Day', 3); 
  $ I -> images_ClientLikeList_ds__data ($ params); 
  $ I -> images_ClientLikeList_ds__graph ($ params); 
 					  ?> 
  }, { 
  <? Php  
  Images_PolicyLikeList_ds $ i = new (); // List like pie consumed by Policy - multiple params possible 
  $ Params = array ('name' => 'images_PolicyLikeList_ds_14d', 'policy' => 'vmware', 'timeago' => 'Day 14', 3); 
  $ I -> images_ClientLikeList_ds__data ($ params); 
  $ I -> images_ClientLikeList_ds__graph ($ params); 
 					  ?> 
  }, { 
  <? Php  		 
  Images_ConsumeHistoricAllDaily $ i = new (); // List consume browse historic chart - multiple params possible 
  $ Params = array ('name' => 'images_ConsumeHistoricAllDaily_24h', 'policy' => 'vmware', 'timeago' => '24 Hour', 'type' => 'hour', 4); 
  $ I -> images_ConsumeHistoricAllDaily__params ($ params); 
  $ I -> images_ConsumeHistoricAllDaily__graph ($ params); 
 					  ?> 
  }, { 
  <? Php  		 
  Images_ConsumeHistoricAllDaily $ i = new (); // List consume browse historic chart - multiple params possible 
  $ Params = array ('name' => 'images_ConsumeHistoricAllDaily_7d', 'policy' => 'vmware', 'timeago' => '7 Day', 'type' => 'day', 4); 
  $ I -> images_ConsumeHistoricAllDaily__params ($ params); 
  $ I -> images_ConsumeHistoricAllDaily__graph ($ params); 
 					  ?> 
  }, { 
  <? Php  		 
  Images_ConsumeHistoricAllDaily $ i = new (); // List consume browse historic chart - multiple params possible 
  $ Params = array ('name' => 'images_ConsumeHistoricAllDaily_14d', 'policy' => 'vmware', 'timeago' => '14 Day', 'type' => 'day', 4); 
  $ I -> images_ConsumeHistoricAllDaily__params ($ params); 
  $ I -> images_ConsumeHistoricAllDaily__graph ($ params); 
 					  ?> 
  }, { 
  <? Php   
  Images_PolicyListInImages_ds $ i = new (); 
  $ Params = array ('name' => 'images_PolicyListInImages_ds_24h', 'policy' => 'vmware', 'timeago' => '24 Hour', 3); 
  $ I -> images_PolicyListInImages_ds__data ($ params); 
  $ I -> images_PolicyListInImages_ds__graph ($ params); 
 					  ?> 
  }, { 
  <? Php   
  Images_PolicyListInImages_ds $ i = new (); 
  $ Params = array ('name' => 'images_PolicyListInImages_ds_7d', 'policy' => 'vmware', 'timeago' => '7 Day', 3); 
  $ I -> images_PolicyListInImages_ds__data ($ params); 
  $ I -> images_PolicyListInImages_ds__graph ($ params); 
 					  ?> 
  }]; 
  // Example basic initialization 
  $ ("#myDashboard") .sDashboard ({ 
  dashboardData: dashboardJSON, 
  disableSelection: false // Enables text selection on the dashboard 
					
  }); 
  // Table row clicked event example 
  $ ("#myDashboard") .bind ("Sdashboardrowclicked", function (e, data) { 
  $ .gritter. Add ({ 
  position: 'bottom-left', 
  title: 'Table Click' 
  time: 1000, 
  text: 'click options widget mode table are not available' 
  }); 
  if (console) { 
  console log ("table row clicked, for widget" + data.selectedWidgetId). 
  } 
  }); 
  // Plot selected event example 
  $ ("#myDashboard") .bind ("Sdashboardplotselected", function (e, data) { 
  $ .gritter. Add ({ 
  position: 'bottom-left', 
  title: 'Selection Box' 
  time: 1000, 
  text: 'The selection options in plot mode widget not available' 
  }); 
  if (console) { 
  console log ("chart range selected, for widget" + data.selectedWidgetId). 
  } 
  }); 
  // Plot example click event 
  $ ("#myDashboard") .bind ("Sdashboardplotclicked", function (e, data) { 
  $ .gritter. Add ({ 
  position: 'bottom-left', 
  title: 'Click Plot' 
  time: 1000, 
  text: 'click options widget plot mode is not available' 
  }); 
  if (console) { 
  console log ("chart clicked, for widget" + data.selectedWidgetId). 
  } 
  }); 
  // Order Changes event widget example 
  $ ("#myDashboard") .bind ("Sdashboardorderchanged", function (e, data) { 
  $ .gritter. Add ({ 
  position: 'bottom-left', 
  title: 'Order Widget' 
  time: 4000, 
  text: 'It has changed the order of the widgets.  Check for more information console.log ' 
  }); 
  if (console) { 
  console log ("Sorted Array."); 
  console log ("+++++++++++++++++++++++++."); 
  console log (data.sortedDefinitions).; 
  console log ("+++++++++++++++++++++++++."); 
  } 
					
  }); 
  }); 
  </ Script> 
  <? Php  
  footer (); 
  $ End = microtime (true); 
  . print "<div id = 'footer' class = 'footer'> Page generated in" round (($ end - $ start), 4) "seconds </ div>."; 
  ?> 
<? Php 
  / * 
  * 
  * Variables Global Super 
  * 
  * / 
  / * 
  * 
  * Js design features added 
  * 
  * / 
  style_menu function () {/ * default style menu * / 
  echo ' 
  <Link rel = "stylesheet" href = "css / slimmenu / style.css" type = "text / css"> 
  <Link rel = "stylesheet" href = "css / slimmenu / slimmenu.css" type = "text / css"> 
  <Style> 
  body { 
  font-family: "Lucida Sans Unicode", "Lucida Console", sans-serif; 
  padding: 0; 
  } 
  a, a: active {text-decoration: none} 
  </ Style> 
  <Script src = "libs / jquery / jquery-1.11.3.min.js"> </ script> 
  <Script src = "libs / slimmenu / jquery.slimmenu.js"> </ script> 
  <Script src = "libs / slimmenu / jquery.easing.min.js"> </ script> '; 
  } 
  style_table function () {/ * default table style * / 
  echo ' 
  <Link rel = "stylesheet" href = "css / jquery / jquery-ui.css" type = "text / css"> 
  <Link rel = "stylesheet" href = "css / datatables / dataTables.jqueryui.min.css" type = "text / css"> 
  <Link rel = "stylesheet" href = "css / datatables / buttons.jqueryui.min.css" type = "text / css"> 
  
  <Script type = "text / javascript" src = "libs / datatables / jquery.dataTables.min.js"> </ script> 
  <Script type = "text / javascript" src = "libs / datatables / dataTables.jqueryui.min.js"> </ script> 
  <Script type = "text / javascript" src = "libs / datatables / dataTables.buttons.min.js"> </ script> 
  <Script type = "text / javascript" src = "libs / datatables / buttons.jqueryui.min.js"> </ script> 
  <Script type = "text / javascript" src = "libs / datatables / jszip.min.js"> </ script> 
  <Script type = "text / javascript" src = "libs / datatables / pdfmake.min.js"> </ script> 
  <Script type = "text / javascript" src = "libs / datatables / vfs_fonts.js"> </ script> 
  <Script type = "text / javascript" src = "libs / datatables / buttons.html5.min.js"> </ script> 
  <Script type = "text / javascript" src = "libs / datatables / buttons.print.min.js"> </ script> 
 		 
  <Script class = "init" type = "text / javascript"> 
  $ (Document) .ready (function () { 
  var table = $ ("# tb"). DataTable ({ 
  "Scrollx": true, 
  "StateSave": true, 
  "PagingType" "full_numbers" 
  "LengthChange": false, 
  select: { 
  style "single" 
  }, 
  "Buttons": [ 
  { 
  extend "copyHtml5" 
  text: "Copy" 
  }, 
  "ExcelHtml5" 
  "CsvHtml5" 
  "PdfHtml5" 
  { 
  extend "print" 
  text: "Print" 
  autoprint: false 
  }, 
  ], 
  "Language": { 
  "EmptyTable", "No data found in the table" 
  "Info": "Showing page _PAGE_ of _PAGES_" 
  "InfoEmpty" "No records found" 
  "InfoFiltered": "(filtered _MAX_ Total records)" 
  "InfoPostFix": "", 
  "Thousands": "", 
  "LengthMenu": "Show records per page _MENU_" 
  "LoadingRecords": "Loading ..." 
  "Processing" "Processing ..." 
  "Search": "Search", 
  "ZeroRecords" "No records found" 
  "Paginate": { 
  "First": "First" 
  "Last": "Ultima" 
  "Next": "Proxima" 
  "Previous", "previous" 
  }, 
  "Aria": { 
  "SortAscending" "enabled to sort in ascending" 
  "SortDescending" "enabled to sort in descending" 
  } 
  } 
  }); 
  table.buttons (). container () 
  .insertBefore ("#tb_filter"); 
  
  }); 
  </ Script> '; 
  } 
  style_dashboard function () {/ * style dashboard menu * / 
	
  echo ' 
  <Link type = "text / css" href = "css / jquery / jquery-ui.css" rel = "stylesheet"> 
  <Link type = "text / css" href = "css / sdashboard / sDashboard.css" rel = "stylesheet"> 
 	 
  <Script src = "libs / jquery / jquery-1.8.3.js" type = "text / javascript"> </ script> 
  <Script src = "libs / jquery / jquery-ui.js" type = "text / javascript"> </ script> 
  <Script src = "libs / datatables / jquery.dataTables.js"> </ script> 
 	 
  <! - [If IE]> 
  <Script language = "javascript" type = "text / javascript" src = "libs / flotr2 / flotr2.ie.min.js"> </ script> 
  <! [Endif] -> 
  <Script src = "libs / flotr2 / flotr2.js" type = "text / javascript"> </ script> 
  <Script src = "libs / sdashboard / jquery-sDashboard.js" type = "text / javascript"> </ script> 
  <Script src = "libs / themeswitcher / jquery.themeswitcher.min.js" type = "text / javascript"> </ script> 
  '; 
  } 
  style_graph function () {/ * default graphics style * / 
  echo ' 
  <Link rel = "stylesheet" href = "css / flotr2 / flotr2.css" type = "text / css"> 
  <Script type = "text / javascript" src = "libs / flotr2 / flotr2.js"> </ script> 
  <Script type = "text / javascript" src = "libs / canvas / flashcanvas.js"> </ script> 
  '; 
  } 
  / * 
  * 
  * Functions of site design 
  * 
  * / 
  menudashboard function () { 
  echo '<ul class = "slimmenu"> 
  <Li> 
  <a href="index.php"> Home </a> 
  </ Li> 
  <Li> 
  Global Dashboard <a href="dashboard.php"> </a> 
  </ Li> 
  <Li> 
  <a href="dashboard-win.php"> Windows </a> 
  </ Li> 
  <Li> 
  <a href="dashboard-sap.php"> SAP </a> 
  </ Li> 
  <Li> 
  <a href="dashboard-unix.php"> UNIX </a> 
  </ Li> 
  <Li> 
  <a href="dashboard-vmware.php"> VMWARE </a> 
  </ Li> 
  <Li> 
  Static href="dashboard-others.php"> <a </a> 
  </ Li> 
  </ Ul> '; 
  } 
  menu function () {/ * drop-down menu * / 
  echo ' 
  <Ul class = "slimmenu"> 
  <Li> 
 <a href="index.php"> Home </a> 
  </ Li> 
  <Li> 
  <a href="dashboard.php"> Dashboard </a> 
  </ Li> 
  <Li> 
  <a href="#"> </a> Customers 
  <Ul> 
  <Li> <a href="index.php?oper=clientname&tx=table&pfunction=client_view"> List </a> </ li> 
  <Li> <a href="#"> Type </a> 
  <Ul> 
  <Li> <a href="index.php?oper=clientname&tx=table&pos=aix"> AIX </a> </ li> 
  <Li> <a href="index.php?oper=clientname&tx=table&pos=virtual"> Virtual </a> </ li> 
  <Li> <a href="index.php?oper=clientname&tx=table&pos=win"> Windows </a> </ li> 
  <Li> <a href="index.php?oper=clientname&tx=table&pos=hp"> HP-UX </a> </ li> 
  </ Ul> 
  </ Li> 
  <Li> <a href="#"> </a> Graphics 
  <Ul> 
  <Li> <a href="index.php?oper=clientname&tx=graph_clientOsLike">% Customers </a> </ li> 
  </ Ul> 
  </ Li> 
  </ Ul> 
  </ Li> 
  <Li> <a href="#"> </a> Policy 
  <Ul> 
  <Li> <a href="index.php?oper=policy&tx=table"> List </a> 
  <Ul> 
  <Li> <a href="index.php?oper=policy&tx=table&pactive=yes"> List Active </a> </ li> 
  <Li> <a href="index.php?oper=policy&tx=table&pactive=no"> Inactive List </a> </ li> 
  </ Ul> 
  </ Li> 
  <Li> <a href="#"> Hosts </a> 
  <Ul> 
  <Li> <a href="index.php?oper=policy&tx=table&pclientname=ssj"> WIN32 </a> </ li> 
  <Li> <a href="index.php?oper=policy&tx=table&pclientname=sap"> SAP </a> </ li> 
  </ Ul> 
  </ Li> 
  <Li> <a href="#"> Type </a> 
  <Ul> 
  <Li> <a href="index.php?oper=policy&tx=table&ptype=oracle"> Oracle </a> </ li> 
  <Li> <a href="index.php?oper=policy&tx=table&ptype=sap"> SAP </a> </ li> 
  <Li> <a href="index.php?oper=policy&tx=table&ptype=sql"> SQL </a> </ li> 
  <Li> <a href="index.php?oper=policy&tx=table&ptype=MS"> Microsoft </a> </ li> 
  <Li> <a href="index.php?oper=policy&tx=table&ptype=vmware"> VMWARE </a> </ li> 
  <Li> <a href="index.php?oper=policy&tx=table&ptype=standard"> Standard </a> </ li> 
  </ Ul> 
  </ Li> 
  <Li> <a href="#"> </a> Graphics 
  <Ul> 
  <Li> <a href="index.php?oper=policy&tx=graph_PolicyTypeLike"> Types (%) </a> </ li> 
  <Li> <a href="index.php?oper=policy&tx=graph_PolicyActive"> Active (%) </a> </ li> 
 		 
  </ Ul> 
  </ Li> 
  </ Ul> 
  </ Li> 
  <Li> <a href="#"> </a> Reports 
  <Ul> 
  <! - <Li> <a href="#"> failed Backups </a> 
  <Ul> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_view&ptimeago=24&pnstatus=1"> </a> 24 hours </ li> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_view&ptimeago=48&pnstatus=1"> </a> 48 hours </ li> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_view&ptimeago=72&pnstatus=1"> </a> 72 hours </ li> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_view&ptimeago=168&pnstatus=1"> </a> 07 Days </ li> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_view&ptimeago=720&pnstatus=1"> </a> 30 Days </ li> 
  </ Ul> 
  </ Li> -> 
  <Li> <a href="#"> Backups </a> 
  <Ul> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_view&ptimeago=12"> 12 Hours </a> </ li> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_view&ptimeago=24"> </a> 24 hours </ li> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_view&ptimeago=48"> </a> 48 hours </ li> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_view&ptimeago=72"> </a> 72 hours </ li> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_view&ptimeago=168"> </a> 07 Days </ li> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_view&ptimeago=720"> </a> 30 Days </ li> 
  </ Ul> 
  </ Li> 
  <Li> <a href="#"> Special </a> 
  <Ul> 
  <Li> <a href="#"> longer </a> 
  <Ul> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_time&ptimeago=24&plimit=10"> </a> 24 hours </ li> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_time&ptimeago=48&plimit=20"> </a> 48 hours </ li> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_time&ptimeago=72&plimit=30"> </a> 72 hours </ li> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_time&ptimeago=168&plimit=70"> </a> 07 Days </ li> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_time&ptimeago=720&plimit=300"> </a> 30 Days </ li> 
  </ Ul> 
  </ Li> 
  <Li> <a href="#"> Mayor Kilobytes </a> 
  <Ul> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_kb&ptimeago=24&plimit=10"> </a> 24 hours </ li> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_kb&ptimeago=48&plimit=20"> </a> 48 hours </ li> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_kb&ptimeago=72&plimit=30"> </a> 72 hours </ li> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_kb&ptimeago=168&plimit=70"> </a> 07 Days </ li> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_kb&ptimeago=720&plimit=300"> </a> 30 Days </ li> 
  </ Ul> 
  </ Li> 
  <Li> <a href="#"> higher average </a> 
  <Ul> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_prom&ptimeago=24&plimit=10"> </a> 24 hours </ li> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_prom&ptimeago=48&plimit=20"> </a> 48 hours </ li> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_prom&ptimeago=72&plimit=30"> </a> 72 hours </ li> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_prom&ptimeago=168&plimit=70"> </a> 07 Days </ li> 
  <Li> <a href="index.php?oper=images&tx=table&pfunction=images_prom&ptimeago=720&plimit=300"> </a> 30 Days </ li> 
  </ Ul> 
  </ Li> 
  </ Ul> 
  </ Li> 
  <Li> <a href="#"> </a> Graphics 
  <Ul> 
  <Li> <a href="?oper=images&tx=graph_consume_all_daily"> </a> historical consumption per day </ li> 
  <Li> <a href="?oper=images&tx=graph_consume_all_hour"> historical consumption per hour </a> </ li> 
  <Li> <a href="?oper=images&tx=graph_consume_1d"> Consumption </a> last 24 hours </ li> 
  </ Ul> 
 		 
  </ Li> 
  </ Ul> 
  </ Li> 
  <Li> <a href="#"> Errors </a> 
  <Ul> 
  <Li> <a href="#"> List Errors </a> 
  <Ul> 
  <Li> <a href="index.php?oper=errors&tx=table&pfunction=errors_view&ptimeago=12&pnstatus=1"> 12 Hours </a> </ li> 
  <Li> <a href="index.php?oper=errors&tx=table&pfunction=errors_view&ptimeago=24&pnstatus=1"> </a> 24 hours </ li> 
  <Li> <a href="index.php?oper=errors&tx=table&pfunction=errors_view&ptimeago=48&pnstatus=1"> </a> 48 hours </ li> 
  <Li> <a href="index.php?oper=errors&tx=table&pfunction=errors_view&ptimeago=72&pnstatus=1"> </a> 72 hours </ li> 
  <Li> <a href="index.php?oper=errors&tx=table&pfunction=errors_view&ptimeago=168&pnstatus=1"> </a> 7 Days </ li> 
  <Li> <a href="index.php?oper=errors&tx=table&pfunction=errors_view&ptimeago=720&pnstatus=1"> </a> 30 Days </ li> 
  </ Ul> 
  </ Li> 
  <Li> <a href="#"> List Backups </a> 
  <Ul> 
  <Li> <a href="index.php?oper=errors&tx=table&pfunction=errors_view&ptimeago=12"> 12 Hours </a> </ li> 
  <Li> <a href="index.php?oper=errors&tx=table&pfunction=errors_view&ptimeago=24"> </a> 24 hours </ li> 
  <Li> <a href="index.php?oper=errors&tx=table&pfunction=errors_view&ptimeago=48"> </a> 48 hours </ li> 
  <Li> <a href="index.php?oper=errors&tx=table&pfunction=errors_view&ptimeago=72"> </a> 72 hours </ li> 
  <Li> <a href="index.php?oper=errors&tx=table&pfunction=errors_view&ptimeago=168"> </a> 7 Days </ li> 
  <Li> <a href="index.php?oper=errors&tx=table&pfunction=errors_view&ptimeago=720"> </a> 30 Days </ li> 
  </ Ul> 
  </ Li> 
  </ Ul> 
  </ Li> 
  <! - <Li> <a href="#"> </a> Graphics 
  <Ul> 
  <Li> <a href="#"> </a> Application Report </ li> 
  <Li> <a href="#"> List </a> Reports </ li> 
  </ Ul> 
  </ Li> 
  <Li> <a href="#"> Other </a> 
  <Ul> 
  <Li> <a href="#"> mail Configuration Report </a> </ li> 
  <Li> <a href="#"> </a> PDF export Settings </ li> 
  </ Ul> 
  </ Li> -> 
  </ Ul> 
  
  <Script> 
  $ ("Ul.slimmenu"). Slimmenu ( 
  { 
  resizeWidth: "800", 
  collapserTitle: "Main Menu" 
  easingEffect "easeInOutQuint" 
  animSpeed: "medium" 
  indentChildren: true, 
  childrenIndenter: "& raquo;" 
  }); 
  </ Script> '; 
  } 
  onloadDashboard function () { 
  echo '<script type = "text / javascript"> 
  $ (Document) .ready (function () { 
 	 
  $ Show () ("slimmenu.."); 
  $ Show () ("headnote.."); 
  $ ("# MyDashboard") show ().; 
  $ Show () ("footer.."); 
  $ Hide () ("spinner.."); 
  }); 
  </ Script> 
  '; 
  } 
  loadWait function () { 
  echo '<div class = "spinner"> 
  <Img src = "css / images / gif-load.gif" alt = "Loading" /> 
  </ Div> '; 
  } 
  / * 
  * 
  * Functions to simplify code of site design 
  * 
  * / 
  starthead function () {/ * open the document * / 
  echo '<! DOCTYPE html> 
  <Html> 
  <Head> 
  '; 
  } 
  endhead function () {/ * open the body * / 
  echo '</ head> 
  <Body> '; 
	
  } 
  footer function () {/ * footer drawing, with all options * / 
  echo ' 
  <Div id = "footer" class = "footer"> 
  <! - [If lte IE 8]> <p> <span style = "filter: fliph; -ms-filter:" fliph "; display: inline-block;"> <! [Endif] -> <span style = "- moz-transform: scaleX (-1) -o-transform: scaleX (-1); -webkit-transform: scaleX (-1) transform: scaleX (-1); display: inline-block; "> & copy;! </ span> <! - [if lte IE 8]> </ span> <! [endif] -> <a CopyLeft href="http://aledec.com.ar"> <abbr >_ aledec </ abbr> </a>, All Wrongs Reversed <script type = "text / javascript"> var d = new Date ();  var year = d.getFullYear ();  document.write (year); </ script> </ p>. 
  </ Div> 
  </ Body> 
  </ Html> '; 
  }; 
  / * 
  * 
  * 
  * 
  * / 
  ?> 
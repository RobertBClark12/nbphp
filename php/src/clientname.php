<? Php  
  / ** 
  * FUNCTIONS 
  * / 
  clientname_table_nb class {// clientname detailed list 
  $ paramsQuery public; 
  public $ result; 
  clientname_table_nb__params function () {/ * used only to replace the parameters sent * / 
  if (isset ($ _ GET ['pclient'])) {/ * customer name to search * / 
  $ CQuery = "AND Client`` LIKE '% "$ _GET. [' Pclient ']"% ".'; 
  $ This -> paramsQuery = $ this -> $ cQuery paramsQuery;. 
  } 
  if (isset ($ _ GET ['post'])) {/ * name * you to search / 
  $ OQuery = '"%." "AND os`` LIKE'% "$ _GET. ['Pos]'; 
  $ This -> paramsQuery = $ this -> $ oQuery paramsQuery;. 
  } 
  if (isset ($ _ GET ['phardware'])) {/ * name * looking hardware / 
  $ HQuery = "AND hardware`` LIKE '% "$ _GET. [' Phardware ']"% ".'; 
  $ This -> paramsQuery = $ this -> $ hQuery paramsQuery;. 
  } 
  if (isset ($ _ GET ['plimit'])) {/ * number of query results, you should go to the end * / 
  $ LQuery = "LIMIT" $ _ GET ['plimit'.]; 
  $ This -> paramsQuery = $ this -> $ lQuery paramsQuery;. 
  } 
  } 
	
  clientname_table_nb__data function () {/ * SQL CONNECTION * / 
  $ Sql = "SELECT * FROM` `Client` Client` WHERE IS NOT NULL" $ this -> paramsQuery.; 
  require 'config-db.php'; / * Configuration Database * / 
  $ This -> result = $ conn -> query ($ sql); / * launched the query * / 
  } 
	
  clientname_table_nb__table function () {/ * drawing table * / 
  if ($ this -> result -> num_rows> 0) { 
  echo "<table id = 'tb' class = 'display' width =" 100% "cellspacing =" 0 "> 
  <Thead> 
  <Tr> 
  <Th> Hardware </ th> 
  <Th> Operating System </ th> 
  <Th> Client Name </ th> 
  </ Tr> 
  </ Thead> 
  <Tfoot> 
  <Tr> 
  <Th> Hardware </ th> 
  <Th> Operating System </ th> 
  <Th> Client Name </ th> 
  </ Tr> 
  </ Tfoot> 
  <Tbody> "; 
  // Output data of each row 
  while ($ row = $ this -> result -> fetch_assoc ()) { 
  echo "<tr> 
  <Td> ". $ Row [" hardware "]." </ Td> 
  <Td> ". $ Row [" I "]." </ Td> 
  <Td> ". $ Row [" client "]." </ Td> 
  </ Tr> "; 
  } 
  echo " 
  </ Tbody> 
  </ Table> "; 
  } Else { 
  echo "0";} 
  } 
  } 
  clientname_oslike_nb class {// clientname you like detailed 
  public $ result_clientos_like_hp = '0'; // assign 0 by default 
  public $ result_clientos_like_aix = '0'; // assign 0 by default 
  public $ result_clientos_like_virtual = '0'; // assign 0 by default 
  public $ result_clientos_like_windows = '0'; // assign 0 by default 
  public $ result_clientos_like_others = '0'; // assign 0 by default 
	
  clientname_oslike_nb__data function () {/ * values ​​for graphic * / 
  include 'config-db.php'; / * Configuration Database * / 
  / * Declare global variables to include in the chart * / 
  / * HP * / 
  $ Sql = "Total SELECT COUNT (*) FROM` Client` as os` WHERE `LIKE '% HP%'"; 
  $ Result = $ conn -> query ($ sql); 
  $ This -> result_clientos_like_hp = $ result -> fetch_assoc (); 
  / * AIX * / 
  $ Sql = "Total SELECT COUNT (*) FROM` Client` as os` WHERE `LIKE '% AIX%'"; 
  $ Result = $ conn -> query ($ sql); 
  $ This -> result_clientos_like_aix = $ result -> fetch_assoc (); 
  / * VIRTUAL * / 
  $ Sql = "Total SELECT COUNT (*) FROM` Client` as os` WHERE `LIKE '% VIRTUAL%'"; 
  $ Result = $ conn -> query ($ sql); 
  $ This -> result_clientos_like_virtual = $ result -> fetch_assoc (); 
  / * WIN * / 
  $ Sql = "Total SELECT COUNT (*) FROM` Client` as os` WHERE `LIKE '% WINDOWS%'"; 
  $ Result = $ conn -> query ($ sql); 
  $ This -> result_clientos_like_windows = $ result -> fetch_assoc (); 
  / * OTHERS * / 
  Total $ sql = "SELECT COUNT (*) FROM` as `os` Client` WHERE NOT LIKE '% HP%' 
  `Os` AND NOT LIKE '% AIX%' AND` os` NOT LIKE '% WIN%' 
  `Os` AND NOT LIKE '% VIRTUAL%'"; 
	
  $ Result = $ conn -> query ($ sql); 
  $ This -> result_clientos_like_others = $ result -> fetch_assoc (); 
  } 
	
  clientname_oslike_nb__draw function () {/ * Graphic * / 
  echo ' 
  <Div id = "flotr2_pie_client_graph_os"> </ div> 
 	 
  <Script type = "text / javascript"> 
  (Function basic_pie (container) { 
 	 
  Flotr.draw graph = (container, [ 
  { 
  data: [[0, '. $ this -> result_clientos_like_hp ["total"].']], 
  label: "HP-UX" 
  }, { 
  data: [[0, '. $ this -> result_clientos_like_aix ["total"].']], 
  label: "AIX" 
  }, { 
  data: [[0, '. $ this -> result_clientos_like_virtual ["total"].']], 
  label: "Virtual" 
  }, { 
  data: [[0, '. $ this -> result_clientos_like_windows ["total"].']], 
  label: "Windows" 
  }, { 
  data: [[0, '. $ this -> result_clientos_like_others ["total"].']], 
  label: "Other" 
  }] 
  { 
  HtmlText: false, 
  grid: { 
  verticalLines: true, 
  horizontalLines: true 
  }, 
  xaxis: { 
  ShowLabels: false 
  }, 
  yaxis: { 
  ShowLabels: false 
  }, 
  foot: { 
 show: true, 
  explode 10 
  }, 
  mouse: { 
  track: true, 
  trackDecimals: 0 
  trackFormatter: function (obj) { 
  return obj.series.label + ":" + obj.y; 
  } 
  }, 
  legend: { 
  position: "se", 
  backgroundColor: "# D2E8FF" 
  }, 
  title: "Clientname Graph" 
  subtitle: "List of customers" 
  }); 
 	 
  }) (Document.getElementById ("flotr2_pie_client_graph_os")); 
  </ Script> '; 
  } 
  } 
  ?> 
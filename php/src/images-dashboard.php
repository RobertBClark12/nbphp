<? Php  
  images_RetentionLevelList_ds class {// List of retention level - multiple params possible 
  public $ result; 
	
  images_RetentionLevelList_ds__data function ($ params) { 
  require 'config-db.php'; 
  if ($ params ['policy'] == 'Any') { 
  $ Sql = 'select ROUND ((sum (kilobytes) / 1024) / 1024) AS gigabyte images from retention_level 
  Client` `WHERE IS NOT NULL AND BACKUP_TIME BETWEEN NOW () - INTERVAL AND NOW () '' $ params [timeago].". ' 
  group by retention_level 
  ORDER BY DESC gigabytes'; 
  } 
  else { 
  $ Sql = 'select ROUND ((sum (kilobytes) / 1024) / 1024) AS gigabyte images from retention_level 
  Client` `WHERE IS NOT NULL AND BACKUP_TIME BETWEEN NOW () - INTERVAL AND NOW () '' $ params [timeago].". ' 
  `Policy` AND LIKE '%'. $ Params ['policy']."% " 
  group by retention_level 
  ORDER BY DESC gigabytes'; 
  } 
  $ This -> result = $ conn -> query ($ sql); 
  } 
	
  images_RetentionLevelList_ds__graph function ($ params) { 
  echo ' 
  widgetTitle "Gigabytes by Retention '." $ params [timeago] - $ params [policy]. "'". '" 
  widgetId ''. "$ params [name]." '" 
  widgetType "chart" 
  widgetContent: { 
  data: ['; 
  if ($ this -> result -> num_rows> 0) { 
  while ($ row = $ this -> result -> fetch_assoc ()) { 
  echo ' 
  { 
  data: [[0] '$ row ["gigabytes"]..'] 
  label: "'. $ row [" retention_level "].'" 
  }, '; 
  } 
  } 
  else { 
  echo '{ 
  data: [[0, 0]], 
  label: "ERROR" 
  }, '; 
  } 
  echo '], 
  options: { 
  HtmlText: false, 
  grid: { 
  verticalLines: false, 
  horizontalLines: false 
  }, 
  xaxis: { 
  ShowLabels: false 
  }, 
  yaxis: { 
  ShowLabels: false 
  }, 
  foot : { 
  show: true, 
  explode: 10, 
  labelFormatter: function (foot, slice) { 
  return slice; 
  } 
  }, 
  mouse: { 
  track: true, 
  trackDecimals: 0 
  trackFormatter: function (obj) { 
  return obj.series.label + ":" + obj.y; 
  } 
  }, 
  legend: { 
  position: "se", 
  backgroundColor: "# D2E8FF" 
  } 
  } 
  } '; 
  } 
	
  } 
  images_ClientImageInImages_ds class {// List of clients in images - multiple params possible 
  $ images_query_graph_in_client_images public; 
  $ images_query_graph_notin_client_images public; 
	
  images_ClientImageInImages_ds__data function ($ params) { 
  require 'config-db.php'; 
  if ($ params ['client'] == 'Any') { 
  Total $ sql = 'SELECT count (*) as from client 
  WHERE IN client 
  ( 
  FROM WHERE client select images BACKUP_TIME BETWEEN NOW () - INTERVAL AND NOW () '' $ params [timeago]. ". ' 
  ); '; 
  $ Result = $ conn -> query ($ sql); / * launched the query * / 
  $ This -> images_query_graph_in_client_images = $ result -> fetch_assoc (); 
  Total $ sql = 'SELECT count (*) as from client 
  NOT IN WHERE client 
  ( 
  FROM WHERE client select images BACKUP_TIME BETWEEN NOW () - INTERVAL AND NOW () '' $ params [timeago]. ". ' 
  ); '; 
  $ Result = $ conn -> query ($ sql); / * launched the query * / 
  $ This -> images_query_graph_notin_client_images = $ result -> fetch_assoc (); 
  } 
  else { 
  Total $ sql = 'SELECT count (*) as from client 
  WHERE IN client 
  ( 
  FROM WHERE client select images BACKUP_TIME BETWEEN NOW () - INTERVAL AND NOW () '' $ params [timeago]. ". ' 
  ) 
  `Client` AND LIKE '%' [client's]% $ params. '.'"; '; 
  $ Result = $ conn -> query ($ sql); / * launched the query * / 
  $ This -> images_query_graph_in_client_images = $ result -> fetch_assoc (); 
  Total $ sql = 'SELECT count (*) as from client 
  NOT IN WHERE client 
  ( 
  FROM WHERE client select images BACKUP_TIME BETWEEN NOW () - INTERVAL AND NOW () '' $ params [timeago]. ". ' 
  ) 
  `Client` AND LIKE '%' [client's]% $ params. '.'"; '; 
  $ Result = $ conn -> query ($ sql); / * launched the query * / 
  $ This -> images_query_graph_notin_client_images = $ result -> fetch_assoc (); 
  } 
  } 
  images_ClientImageInImages_ds__graph function ($ params) { 
	
  echo ' 
  widgetTitle: "Percentage of Clients Backup '." "." $ params [timeago] - $ params [client]. "'" 
  widgetId ''. "$ params [name]." '" 
  widgetType "chart" 
  widgetContent: { 
  data: [ 
  { 
  data: [[0, '. $ this -> images_query_graph_in_client_images ["total"].']], 
  label: "With Backup" 
  }, { 
  data: [[0, '. $ this -> images_query_graph_notin_client_images ["total"].']], 
  label: "No Backup" 
  foot : { 
  explode 10 
  } 
  }] 
  options: { 
  HtmlText: false, 
  grid: { 
  verticalLines: false, 
  horizontalLines: false 
  }, 
  xaxis: { 
  ShowLabels: false 
  }, 
  yaxis: { 
  ShowLabels: false 
  }, 
  foot : { 
  show: true, 
  explode: 6 
  }, 
  mouse: { 
  track: true, 
  trackDecimals: 0 
  trackFormatter: function (obj) { 
  return obj.series.label + ":" + obj.y; 
  } 
  }, 
  grid: { 
  hoverable: false, 
  clickable: false 
  }, 
  legend: { 
  position: "se", 
  backgroundColor: "# D2E8FF" 
  } 
  } 
  } '; 
	
  } 
  } 
  images_PolicyListInImages_ds class {// List of policys.But in images - multiple params possible 
  public $ result; 
  images_PolicyListInImages_ds__data function ($ params) { 
  require 'config-db.php'; 
  if ($ params ['policy'] == 'Any') { 
  $ Sql = 'SELECT name ``, `type',` `client from policies 
  WHERE active = "YES" 
  AND NOT LIKE `` type '% Vault% " 
  Name AND NOT IN (SELECT FROM images WHERE policy BACKUP_TIME BETWEEN NOW () - '.. "$ Params [timeago]"' INTERVAL AND NOW () 
  ); '; 
  $ This -> result = $ conn -> query ($ sql); / * launched the query * / 
  } 
  else { 
  $ Sql = 'SELECT name ``, `type',` `client from policies 
  WHERE active = "YES" 
  AND NOT LIKE `` type '% Vault% " 
  `` AND name LIKE '%'. $ Params ['policy']. "%" 
  Name AND NOT IN (SELECT FROM images WHERE policy BACKUP_TIME BETWEEN NOW () - '.. "$ Params [timeago]"' INTERVAL AND NOW () 
  ); '; 
  $ This -> result = $ conn -> query ($ sql); / * launched the query * / 
  } 
  } 
  images_PolicyListInImages_ds__graph function ($ params) { 
  echo 'widgetTitle: "List of Policys without Backup.'" "-".. '"" $ params [timeago] $ params [policy] 
  widgetId ''. "$ params [name]." '" 
  widgetType: "table" 
  setJqueryStyle: true, 
  widgetContent: { 
  "AaData" ['; 
  if ($ this -> result -> num_rows> 0) { 
  while ($ row = $ this -> result -> fetch_assoc ()) { 
  echo "['". $ row [' name ']. "', '". $ row [' type ']. "', '". $ row [' client ']. "'], 
  "; 
  } 
  echo '],'; 
  } 
  else { 
  echo "['No', 'results', '']], 
  "; 
  } 
  echo '' aoColumns ": [{ 
  "STitle" "Policy" 
  }, { 
  "STitle": "Type" 
  }, { 
  "STitle": "Client" 
  }] 
  "ALengthMenu" [[1, 25, 50, -1], [1, 25, 50, "All"]], 
  "IDisplayLength": 50, 
  "BPaginate": true, 
  "BAutoWidth": true 
  } '; 
  } 
  } 
  images_ClientListInImages_ds class {// List of clients in images - only possible timeago params 
  public $ result; 
  images_ClientListInImages_ds__data function ($ params) { 
  require 'config-db.php'; 
  $ Sql = 'SELECT client ``, `` hardware from client 
  Client WHERE NOT IN (SELECT FROM images WHERE BACKUP_TIME client BETWEEN NOW () - INTERVAL AND NOW () '' $ params [timeago]. ". ' 
  ); '; 
  $ This -> result = $ conn -> query ($ sql); / * launched the query * / 
  } 
  images_ClientListInImages_ds__graph function ($ params) { 
  echo 'widgetTitle: "List of Customers without Backup.'" '"." $ params [timeago] 
  widgetId ''. "$ params [name]." '" 
  widgetType: "table" 
  setJqueryStyle: true, 
  widgetContent: { 
  "AaData" ['; 
  if ($ this -> result -> num_rows> 0) { 
  while ($ row = $ this -> result -> fetch_assoc ()) { 
  echo "['". $ row [' client ']. "', '". $ row [' hardware ']. "'], 
  "; 
  } 
  echo '],'; 
  } 
  else { 
  echo "['No', 'results']], 
  "; 
  } 
  echo '' aoColumns ": [{ 
  "STitle": "Client" 
  }, { 
  "STitle" "Hardware" 
  }] 
  "ALengthMenu" [[1, 25, 50, -1], [1, 25, 50, "All"]], 
  "IDisplayLength": 50, 
  "BPaginate": true, 
  "BAutoWidth": true 
  } '; 
  } 
  } 
  images_ConsumeClientList_ds class List {// consume of clients - multiple params possible 
  public $ result; 
	
  images_ConsumeClientList_ds__data function ($ params) { 
  require 'config-db.php'; 
  if ($ params ['policy'] == 'Any') { 
  $ Sql = 'select client, 
  ROUND ((sum (kilobytes) / 1024/1024), 2) AS gigabytes, 
  ROUND (SUM (elapsed_time) / 60) as minutes, 
  (Format ((SUM (kilobytes) / 1024) / SUM (elapsed_time), 2)) AS mbs, 
  count (*) AS images 
  from images 
  WHERE BACKUP_TIME BETWEEN NOW () - INTERVAL AND NOW () '' $ params [timeago]. ". ' 
  group by client order by DESC gigabytes; '; 
  } 
  else { 
  $ Sql = 'select client, 
  ROUND ((sum (kilobytes) / 1024/1024), 2) AS gigabytes, 
  ROUND (SUM (elapsed_time) / 60) as minutes, 
  (Format ((SUM (kilobytes) / 1024) / SUM (elapsed_time), 2)) AS mbs, 
  count (*) AS images 
  from images 
  WHERE BACKUP_TIME BETWEEN NOW () - INTERVAL AND NOW () '' $ params [timeago]. ". ' 
  `Policy` AND LIKE '%'. $ Params ['policy']."% " 
  group by client order by DESC gigabytes; '; 
  } 
  $ This -> result = $ conn -> query ($ sql); 
  } 
	
  images_ConsumeClientList_ds__table function ($ params) { 
  echo 'widgetTitle: "Consumption Client Backups.'" "-".. '"" $ params [timeago] $ params [policy] 
  widgetId ''. "$ params [name]." '" 
  widgetType: "table" 
  setJqueryStyle: true, 
  widgetContent: { 
  "AaData" ['; 
  if ($ this -> result -> num_rows> 0) { 
  while ($ row = $ this -> result -> fetch_assoc ()) { 
  echo "['". $ row [' client ']. "', '". $ row [' gigabyte ']. "', '". $ row [' Minutes']. "','". $ row ['MBS']. "','". $ row ['images']. "'], 
  "; 
  } 
  echo '],'; 
  } 
  else { 
  echo "['No', 'results', '', '', '', '']], 
  "; 
  } 
  echo '' aoColumns ": [{ 
  "STitle": "Client" 
  }, { 
  "STitle": "Gigabytes" 
  }, { 
  "STitle", "Minutes" 
  }, { 
  "STitle": "MB / S" 
  }, { 
  "STitle": "Images" 
  }] 
  "ALengthMenu" [[1, 25, 50, -1], [1, 25, 50, "All"]], 
  "IDisplayLength": 50, 
  "BPaginate": true, 
  "BAutoWidth": true 
  } '; 
  } 
  } 
  images_ConsumePolicyList_ds class List {// consume of policys.But - multiple params possible 
  public $ result; 
	
  images_ConsumePolicyList_ds__data function ($ params) { 
  require 'config-db.php'; 
		
  if ($ params ['policy'] == 'Any') { 
  $ Sql = 'select policy, 
  ROUND ((sum (kilobytes) / 1024/1024), 2) AS gigabytes, 
  ROUND (SUM (elapsed_time) / 60) as minutes, 
  (Format ((SUM (kilobytes) / 1024) / SUM (elapsed_time), 2)) AS mbs 
  from images 
  WHERE BACKUP_TIME BETWEEN NOW () - INTERVAL AND NOW () '' $ params [timeago]. ". ' 
  group by policy; '; 
  } 
  else { 
  $ Sql = 'select policy, 
  ROUND ((sum (kilobytes) / 1024/1024), 2) AS gigabytes, 
  ROUND (SUM (elapsed_time) / 60) as minutes, 
  (Format ((SUM (kilobytes) / 1024) / SUM (elapsed_time), 2)) AS mbs 
  from images 
  WHERE BACKUP_TIME BETWEEN NOW () - INTERVAL AND NOW () '' $ params [timeago]. ". ' 
  `Policy` AND LIKE '%'. $ Params ['policy']."% " 
  group by policy; '; 
  } 
  $ This -> result = $ conn -> query ($ sql); 
  } 
  images_ConsumePolicyList_ds__table function ($ params) { 
  echo 'widgetTitle: "Consumer Policy'". "-".. '"" $ params [timeago] $ params [policy] 
  widgetId ''. "$ params [name]." '" 
  widgetType: "table" 
  setJqueryStyle: true, 
  widgetContent: { 
  "AaData" ['; 
  if ($ this -> result -> num_rows> 0) { 
  while ($ row = $ this -> result -> fetch_assoc ()) { 
  echo "['". $ row [' policy ']. "', '". $ row [' gigabyte ']. "', '". $ row [' Minutes']. "','". $ row ['MBS']. "'], 
  "; 
  } 
  echo '],'; 
  } 
  else { 
  echo "['No', 'results', '', '', '', '']], 
  "; 
  } 
  echo '' aoColumns ": [{ 
  "STitle" "Policy" 
  }, { 
  "STitle": "Gigabytes" 
  }, { 
  "STitle", "Minutes" 
  }, { 
  "STitle": "MB / S" 
  }] 
  "ALengthMenu" [[1, 25, 50, -1], [1, 25, 50, "All"]], 
  "IDisplayLength": 50, 
  "BPaginate": true, 
  "BAutoWidth": true 
  } '; 
  } 
	
  } 
  images_ListGroupHour_ds class List {// consumed by hour - multiple params possible 
  public $ result; 
	
  images_ListGroupHour_ds__data function ($ params) { 
  require 'config-db.php'; 
  if ($ params ['policy'] == 'Any') { 
  $ Sql = 'select hour (BACKUP_TIME) AS hour, 
  date (BACKUP_TIME) AS day, 
  ROUND ((sum (kilobytes) / 1024/1024), 2) AS gigabytes, 
  (Format (SUM (kilobytes) / SUM (elapsed_time), 2)) AS mbs, 
  count (*) AS images 
  WHERE images from BACKUP_TIME BETWEEN NOW () - INTERVAL AND NOW () '' $ params [timeago]. ". ' 
  group by hour (BACKUP_TIME) 
  ORDER BY day, hour; '; 
  } 
  else { 
  $ Sql = 'select hour (BACKUP_TIME) AS hour, 
  date (BACKUP_TIME) AS day, 
  ROUND ((sum (kilobytes) / 1024/1024), 2) AS gigabytes, 
  (Format (SUM (kilobytes) / SUM (elapsed_time), 2)) AS mbs, 
  count (*) AS images 
  WHERE images from BACKUP_TIME BETWEEN NOW () - INTERVAL AND NOW () '' $ params [timeago]. ". ' 
  `Policy` AND LIKE '%'. $ Params ['policy']."% " 
  group by hour (BACKUP_TIME) 
  ORDER BY day, hour; '; 
  } 
  $ This -> result = $ conn -> query ($ sql); / * launched the query * / 
  } 
	
  images_ListGroupHour_ds__table function ($ params) { 
  echo 'widgetTitle: "Consumption and Times Backups.'" "-".. '"" $ params [timeago] $ params [policy] 
  widgetId ''. "$ params [name]." '" 
  widgetType: "table" 
  setJqueryStyle: true, 
  widgetContent: { 
  "AaData" ['; 
  global $ result; / * up the global variable * / 
  if ($ this -> result -> num_rows> 0) { 
  while ($ row = $ this -> result -> fetch_assoc ()) { 
  echo "['". $ row [' hour ']. "', '". $ row [' gigabyte ']. "', '". $ row [' MBS ']. "', '". $ row ['you images']. "'], 
  "; 
  } 
  echo '],'; 
  } 
  else { 
  echo "['No', 'results', '', '', '', '']], 
  "; 
  } 
  echo '' aoColumns ": [{ 
  "STitle", "Hour" 
  }, { 
  "STitle": "Gigabytes" 
  }, { 
  "STitle": "MB / s" 
  }, { 
  "STitle": "Images" 
  }] 
  "ALengthMenu" [[1, 25, 50, -1], [1, 25, 50, "All"]], 
  "IDisplayLength": 25, 
  "BPaginate": true, 
  "BAutoWidth": true 
  } '; 
  } 
	
  } 
  images_PolicyLikeList_ds class List {// like pie consumed by Policy - multiple params possible 
  public $ result; 
	
  images_ClientLikeList_ds__data function ($ params) { 
  require 'config-db.php'; 
  if ($ params ['policy'] == 'Any') { 
  $ Sql = 'select ROUND ((sum (kilobytes) / 1024/1024)) AS gigabytes, 
  client 
  from images 
  Client` `WHERE IS NOT NULL AND 
  BACKUP_TIME BETWEEN NOW () - '.. "$ params [timeago]"' INTERVAL AND NOW () 
  group by client 
  ORDER BY DESC gigabytes 
  LIMIT 30 '; 
  } 
  else { 
  $ Sql = 'select ROUND ((sum (kilobytes) / 1024/1024)) AS gigabytes, 
  client 
  from images 
  Client` `WHERE IS NOT NULL AND 
  BACKUP_TIME BETWEEN NOW () - '.. "$ params [timeago]"' INTERVAL AND NOW () 
  `Policy` AND LIKE '%'. $ Params ['policy']."% " 
  group by client 
  ORDER BY DESC gigabytes'; 
  } 
  $ This -> result = $ conn -> query ($ sql); 
  } 
  images_ClientLikeList_ds__graph function ($ params) { 
  echo ' 
  widgetTitle "Gigabytes per customer '." $ params [timeago] - $ params [policy]. "'". '" 
  widgetId ''. "$ params [name]." '" 
  widgetType "chart" 
  widgetContent: { 
  data: ['; 
  if ($ this -> result -> num_rows> 0) { 
  while ($ row = $ this -> result -> fetch_assoc ()) { 
  echo ' 
  { 
  data: [[0] '$ row ["gigabytes"]..'] 
  label: "'. $ row [" client "].'" 
  }, '; 
  } 
  } 
  else { 
  echo '{ 
  data: [[0, 0]], 
  label: "ERROR" 
  }, '; 
  } 
  echo '], 
  options: { 
  HtmlText: false, 
  grid: { 
  verticalLines: false, 
  horizontalLines: false 
  }, 
  xaxis: { 
  ShowLabels: false 
  }, 
  yaxis: { 
  ShowLabels: false 
  }, 
  foot : { 
  show: true, 
  explode: 6, 
  labelFormatter: function (foot, slice) { 
  return slice; 
  } 
  }, 
  mouse: { 
  track: true, 
  trackDecimals: 0 
  trackFormatter: function (obj) { 
  return obj.series.label + ":" + obj.y; 
  } 
  }, 
  legend: { 
  position: "se", 
  backgroundColor: "# D2E8FF" 
  } 
  } 
  } '; 
  } 
	
  } 
  images_ConsumeHistoricAllDaily class List {// consume browse historic chart - multiple params possible 
  public $ result; 
  public $ type; 
  $ timeformat public; 
  images_ConsumeHistoricAllDaily__params function ($ params) { 
  switch ($ params ['type']) {// switch for different date format filter 
  case "hour": 
  $ This -> type = "% Y-% m-% d% H: 00: 00"; 
  $ This -> timeformat = "% h hs"; 
  break; 
  case "day": 
  $ This -> type = "% Y-% m-% d"; 
  $ This -> timeformat = "% d-% b"; 
  break; 
  case "month": 
  $ This -> type = "% Y-% m"; 
  $ This -> timeformat = "% d-% b"; 
  break; 
  } 
  } 
  images_ConsumeHistoricAllDaily__data function ($ params) { 
  require 'config-db.php'; 
  if ($ params ['policy'] == 'Any') { 
  $ Sql = 'SELECT UNIX_TIMESTAMP (CONVERT_TZ (DATE_FORMAT (BACKUP_TIME,' '$ this -..> Type' ')' + 00:00 ',' - 04:00 ')) AS date, 
  ROUND ((sum (kilobytes) / 1024/1024), 0) AS gigabytes 
  FROM images 
  WHERE BACKUP_TIME BETWEEN NOW () - INTERVAL AND NOW () '' $ params [timeago]. ". ' 
  GROUP BY date 
  ORDER BY date ASC; '; 
  } 
  else { 
  $ Sql = 'SELECT UNIX_TIMESTAMP (CONVERT_TZ (DATE_FORMAT (BACKUP_TIME,' '$ this -..> Type' ')' + 00:00 ',' - 04:00 ')) AS date, 
  ROUND ((sum (kilobytes) / 1024/1024), 0) AS gigabytes 
  FROM images 
  WHERE BACKUP_TIME BETWEEN NOW () - INTERVAL AND NOW () '' $ params [timeago]. ". ' 
  `Policy` AND LIKE '%'. $ Params ['policy']."% " 
  GROUP BY date 
  ORDER BY date ASC; '; 
  } 
  $ This -> result = $ conn -> query ($ sql); 
  } 
  images_ConsumeHistoricAllDaily__js function ($ params) { 
  echo '<script type = "text / javascript"> 
  '. "$ Params [name]".' = Function () { 
  var 
  d2 = [] 
  start = new Date ("2013/01/01 00:00"). getTime () 
  options, graph, i, x, or; 
  '; 
	
  if ($ this -> result -> num_rows> 0) { 
  while ($ row = $ this -> result -> fetch_assoc ()) { 
  echo 'd2.push (['. $ row [$ "date"]. ''. row ["gigabytes"]. ']) 
  '; 
  } 
  } Else { 
  echo 'd2.push ([0, 0])'; 
  } 
  echo ' 
  return [d2]; 
  }; 
  </ Script> '; 
  } 
  images_ConsumeHistoricAllDaily__graph function ($ params) { 
  echo ' 
  widgetTitle "historical consumption '." $ params [timeago] - $ params [policy]. "'". '" 
  widgetId ''. "$ params [name]." '" 
  widgetType "chart" 
  getDataBySelection: true, 
  widgetContent: { 
  data: (), '' $ params [name] ".. ' 
  options: { 
  xaxis: { 
  mode, "time", 
  timeFormat ''. "$ this -> timeformat". '" 
  TIMEUNIT "second" 
  labelsAngle: 45 
  }, 
  grid: { 
  minorVerticalLines: true 
  }, 
  yaxis: { 
  title: "Gigabytes" 
  }, 
  selection: { 
  mode: "x" 
  }, 
  mouse: { 
  track: true, 
  trackDecimals: 0 
  trackFormatter: function (obj) { 
  return obj.x + ":" + obj.y; 
  } 
  }, 
  HtmlText: false 
  } 
  } '; 
  } 
	
  } 
  ?> 